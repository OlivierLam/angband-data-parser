import parser from './src/parser'
import evaluate from './src/evaluate'

import { readFileSync, writeFileSync } from 'fs'


const gamedata = readFileSync('./gamedata/monster.txt', 'utf8')
let result = evaluate(parser(gamedata))

console.log(result)