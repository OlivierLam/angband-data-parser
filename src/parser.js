import P from 'parsimmon'

/* my Parser helper */
const mySepBy1 = (parser, separator) => P.seq(P.seq(parser, separator).map(x => x[0]).atLeast(1), parser).map(x => [...x[0], x[1]])
P.prototype.mySepBy1 = function (separator) {
    return mySepBy1(this, separator)
}
/* Common Parser */
const value = 
    P.regexp(/[0-9A-Za-zÀ-ÿ\-\_\[\]\ \,\*]+/)
    .desc('Value')

const string = 
    P.regexp(/[\ -~À-ÿ]+/)
    .desc('String')

const stringComment = 
    P.regexp(/[\ -~À-ÿ]*/)
    .desc('String Comment')

/* Data Parser */
const variable = 
    P.optWhitespace.then(
        P.seq(value, P.string(':')
    ).map(x => x[0]))
    .desc('Variable')

const object = value
    .mySepBy1(P.string(':'))
    .desc('Object')

const arraySeparator = 
    P.alt(P.string('|'),P.string(' | '))

const array = value
    .mySepBy1(arraySeparator).map(x => x.map(s => s.trim()))
    .desc('Array')

const data = P.alt(
    object,
    array,
    string
).desc('Data')

const dataLine = P.seq(
    variable,
    data
).desc('Line')

const lines = P.alt(
    dataLine,
    P.seq(P.string('#'), stringComment),
    P.string('')
).sepBy1(P.string("\n"))

const dataParser = 
    P.optWhitespace.then(lines)
    .skip(P.optWhitespace)

export default function parser(gamedata) {
    return dataParser.tryParse(gamedata)
}

export const _test = {
    _mySepBy1 : mySepBy1,
    _value : value,
    _string : string,
    _stringComment : stringComment,
    _variable : variable,
    _object : object,
    _array : array,
    _data : data,
    _dataLine : dataLine,
    _lines : lines,
    _dataParser : dataParser
}

