import { expect } from 'chai'
import { _test as my }  from '../src/parser' 

/* Test Fixtures */
const exemple = {
    array : { 
        str : "test | of|my|array",
        result : ['test', 'of', 'my', 'array']
    },
    object : { 
        str :  "test:o f:[my]:object",
        result : ['test', 'o f', '[my]', 'object']
    },
    string : {
        str : "hello world.",
        result : "hello world."
    }
}

const realExemple = `
name:scruffy little dog
base:canine
color:U
speed:110
hit-points:2
hearing:20
smell:50
armor-class:1
sleepiness:5
depth:0
rarity:3
experience:0
blow:BITE:HURT:1d1
flags:RAND_25
desc:A dark elven figure in armour and radiating evil power.
desc:A thin flea-ridden mutt growling as you get close.`

describe("Parser", () => {
    it("Object", () => {
        let result = my._object.parse(`${exemple.object.str}`)
        expect(result.value).to.eql(exemple.object.result)
    })
    it("Array", () => {
        let result = my._array.parse(`${exemple.array.str}`)
        expect(result.value).to.eql(exemple.array.result)
    })
    it("String", () => {
        let result = my._string.parse(`${exemple.string.str}`)
        expect(result.value).to.eql(exemple.string.result, JSON.stringify(result))
    })
    it("Variable", () => {
        let result = my._variable.parse(`variable:`)
        expect(result.value).to.eql('variable')
    });
    it("alt Array, Object ", () => {

        let result = my._data.parse(`${exemple.array.str}`)
        expect(result.value).to.eql(exemple.array.result)
        let result2 = my._data.parse(`${exemple.object.str}`)
        expect(result2.value).to.eql(exemple.object.result)
    });
    it("Line with the variable", () => {
        let result = my._dataLine.parse(`variable:${exemple.array.str}`)
        expect(result.value).to.eql(['variable', exemple.array.result], JSON.stringify(result))
    });
    it("Parse multiline with variable and array or object", () => {
        let result = my._dataParser.parse(`
            object:${exemple.object.str}
            array:${exemple.array.str}
            string:${exemple.string.str}`)
        expect(result.value).to.eql([
            ['object', exemple.object.result],
            ['array', exemple.array.result],
            ['string', exemple.string.result]
        ], JSON.stringify(result))
    });
    it("Parse real exemple", () => {
        let result = my._dataParser.parse(realExemple)
        expect(result.status).to.eq(true, JSON.stringify(result))
    })
})