import * as R from 'ramda'

export default function evaluate(ast) {

    return ast.reduce((accu, line) => {
        /* Empty */
        if (line === '') return accu
        /* Comment */
        if (line[0] === '#') return accu

        /* Data */
        let key = line[0];
        let data = line.slice(1)
        if (key === 'name') {
            if (accu.current !== null) {
                R.map((k) => {
                    accu.current[k] = accu.current[k].length === 1 ? accu.current[k][0] : accu.current[k]
                }, R.keys(accu.current))
                accu.result.push({...accu.current})
            }
            accu.current = {}
        }

        if (accu.current[key] === undefined) {
            accu.current[key] = data
        } else {
            accu.current[key] = [...accu.current[key], ...data]
        }
        return accu

    }, { result : [], current : null })
}